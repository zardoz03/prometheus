(add-to-load-path (getcwd))
(map load '("avector.scm" "hermes.scm" "prometheus.scm" "prometheus-x.scm"))

(use-modules
 (avector)
 (hermes)
 (srfi srfi-9)
 (prometheus) (prometheus-x))
