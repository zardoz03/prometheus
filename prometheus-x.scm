(add-to-load-path (getcwd))
(define-module (prometheus-x)
  #:use-module (prometheus)
  #:use-module (srfi srfi-1)
  #:use-module (hermes)
  #:export (cascade
            constant-message
            comment
            wrap-method
            *slot-setters*))

;;; VALUE SLOT WRAPPER
(define *slot-setters*
  (let ([o (*object* 'clone)])

    (o 'add-value-slot! 'default 'default: 'bang)
    (o 'add-value-slot! 'selectors 'selectors:
       '((kwarg . "~a:")
         (bang  . "set-~a!")))

    (let* ((method 'selectors:)
           (selectors:-proc (and=> (assoc method (messages-alist (o 'messages))) cdr)))

      ;(format #t "#messages   [~a]~%" (messages-alist (o 'messages)))
      ;(format #t "#selectors: [~a]~%" selectors:-proc)
      (o 'delete-message! method)
      (o 'add-method-slot! 'add-selector:
         (lambda (self resend elt)
           (let ((ret (cons elt (self 'selectors))))
             (selectors:-proc self resend ret)
             ret)))

      (o 'add-method-slot! 'remove-selector:
         (lambda (self resend elt-or-pred)
           (let ((ret
                  (remove (if (procedure? elt-or-pred)
                              elt-or-pred
                              (lambda (x) (equal? (car x) elt-or-pred)))
                          (self 'selectors))))
             (selectors:-proc self resend ret)
             ;; choose first in list as default if its not present.
             (unless (assoc (self 'default) ret)
               (self 'default: (car (self 'styles))))
             ret))))

    (o 'add-method-slot! 'styles
       (lambda (self resend)
         (map car (self 'selectors))))

    (o 'add-method-slot! 'member:
       (lambda (self resend elt)
         (let ((m (member elt (self 'styles))))
           (if m (car m) m))))

    (o 'add-method-slot! 'selector-fmt:
       (lambda (self resend elt)
         (cdr (assoc (if (self 'member: elt)
                         elt
                         (self 'default))
                     (self 'selectors)))))

    o))

;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; add-value-slots:
;;
;; (o1 'add-value-slots: ((foo mut 0)) 'style: 'kwarg)
;; => creates 'foo 'foo:
(*object* 'add-method-slot! 'add-value-slots:
  (let* ((main
          '())
         (entry
          (case-lambda
            ((self slot-resender slot-alist)
             (main self slot-resender slot-alist #f #f))

            ((self slot-resender slot-alist style-resend getter-style)
             (main self slot-resender slot-alist style-resend getter-style)))))

    (set! main
     (lambda (self slot-resender slot-alist style-resend getter-style)
       (let* ([getter-style (and style-resend getter-style)])
         (map (lambda (s)
                (let* ([slot-name (car s)]
                       [style-fmt
                        ;;getter-style if #<void> will return *slot-setters*.default
                        (*slot-setters* 'selector-fmt: getter-style)]
                       [slot-setter
                        (string->symbol
                         (format #f style-fmt
                                 (symbol->string slot-name)))]
                       [slot-value
                        (if (eq? 'mut (list-ref s 1))
                            (list slot-setter (list-ref s 2))
                            (cdr s))])
                  (apply self
                         `(add-value-slot! ,slot-name
                           ,@slot-value))
                  (cons slot-name slot-value)))
              slot-alist))))
    entry))

;;; IMPLEMENTOR MESSAGE INTERFACE
(*object* 'add-value-slot! 'implementor-messages 'implementor-messages: '())

;; should we make this an alist of specifier -> message ? 
(*object* 'add-method-slot! 'add-implementor-message:
  (lambda (self resender message)
    (self 'implementor-messages:
          (cons message
                (self 'implementor-messages)))
    (self 'add-method-slot! message
          (lambda (self2 resend2)
            (self2 'clone-responsibility message)))))

(*object* 'add-method-slot! 'implement!
  (lambda (self resender message block)
    (self 'implementor-messages: (remove (lambda (x) (eq? message x))
                                         (self 'implementor-messages)))
    (self 'add-method-slot! message block)))

(*object* 'add-method-slot! 'check-if-all-implemented
  (lambda (self resend)
    (or (null? (self 'implementor-messages))
        (format #f "There are unimplemented messages on the object ~a: ~a"
                (self 'name)
                (self 'implementor-messages)))))

(*object* 'add-method-slot! 'add-implementor-messages:
  (lambda (self resend messages)
    (map (lambda (m)
           (self 'add-implementor-message: m))
         messages)
    #||#))

;;; CLONE METHOD RESPONSIBILITIES
(*object* 'add-method-slot! 'clone-responsibility
  (lambda (self resend message)
    (error "Message is clone's responsibility to implement" self message)))

(*object* 'add-method-slot! 'clone-method-responsibility
  (lambda (self resend message)
    (self 'add-implementor-message: message)
    (self 'add-method-slot! message
          (lambda (self . _) (self 'clone-responsibility message)))
    message))

(*object* 'add-method-slot! 'clone-method-responsibilities:
  (lambda (self resend messages)
    (map (lambda (m) (self 'clone-method-responsibility m) m)
         messages)))

;;; NAME PRINTER
(*object* 'add-value-slot! 'name 'name: "*object*")
;; wrap original name:
(let* ((method 'name:)
       (base-setter
        ;; we cannot reference the `self name' method in this procedure
        ;; therefore we steal it from the message list
        (cdr (assoc method (messages-alist (*object* 'messages))))))
  ;(format #t "~%~%base-setter: ~a~%" base-setter)
  ;(format #t "method: ~a~%" method)

  (*object* 'delete-message! method)
  (*object* 'add-method-slot! method
     (lambda (self resend new)
       (base-setter self resend new)
       (set-procedure-property! self 'name (string->symbol new))
       ;; workaround because apparently calls copy the Original method to
       ;; themselves without the copy-method! expr in the older impl of
       ;; make-getter-setter
       ;(unless (eq? *object* self) (self 'delete-message! method))
       new)))

(*object* 'name: "*object*")

(define-syntax wrap-method
  (syntax-rules ()
   ((_ (object method) baseproc)
    (let ((methodproc
           (cdr
            (assoc method
                   (messages-alist
                    (object 'messages))))))
      (object 'delete-message! method)
      (object 'add-method-slot! method
              (apply baseproc
                     (list methodproc)))))))

#||
(wrap-method (*object* 'name:)
  (lambda (baseproc)
    (lambda (self resend new)
      (baseproc self resend new)
      (set-procedure-property! self 'name (string->symbol new)))))
||#

;;; Cascading
(*object* 'add-method-slot! 'yourself (lambda (self resend) self))

(define (cascade object messages)
  (let lp ((o object)
           (m messages)
           (ret '()))
    (cond
     [(null? m) o]
     [else #;(format #t "---~%o:=~a~%m.car:=~a~%" o (car m))
      (let* ((msg
              (if (and #t
                       ;;;; this should do arity checking ideally.
                       ;; (1- (car (procedure-minimum-arity (cdr (assoc (car m) (o 'messages))))))
                       ;;;; this is non-standard and relies on the guile-specific
                       ;;;; procedure `procedure-minimum-arity'
                       (not (list? (car m))))
                  (list (car m))
                  ;;^~~~~~~~~~~~~^
                  ;; only supposed to be unary messages
                  (car m)))
             (ret (apply o msg)))
        ;; make sure the return value is an object for all except the last value
        (lp (if (or (procedure? ret) (= (length m) 1))
                ret
                o)
            (cdr m)
            ret))])))

(define-syntax comment 
  (syntax-rules () 
    [[_ ...] 
     '()]))
(comment
 (cascade *object*
          `(clone
            (add-method-slot! go-fuck ,(lambda (self resender) 'yourself))
            go-fuck))
 ===> 'yourself)

(define (constant-message object)
  (lambda (self resend) 
    object))

(define-syntax entry-point
  (lambda (stx)
    (syntax-case stx (:default :base-args)
      ;; base case
      [(_ (entry-points)
          :base-args (base-args)
          body ...)
       #'(entry-point (entry-points :default #f)
          :base-args (base-args)
          body ...)]
      ;; proper macro
      [(_ (entry-points :default defaults)
          :base-args (base-args)
          body ...)
       (with-syntax ((expanded-entry-points
                       (datum->syntax #'entry-points '())))
         #`(let ((main '())
                 (entry (case-lambda expanded-entry-points)))
             (set! main (lambda base-args body ...))
             entry))])))

;; basically ,@(mapcar myfun entry-points) and cons 'case-lambda to it,
;; where myfun basically just diffs entry-points and base-args and replaces it with defaults.
;; defaults should be either Nil/'()/#f/#nil/wtv the fuck it needs to be for the
;; end user
