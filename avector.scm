;; type assc = 'a 'b cons
;; type avec = assc vec 

;; ref  :: assc -> 'a | 'b
;; test :: assc -> bool

;;; NOTE: this isnt to say that association vectors aren't required to have 
;;; the same types for keys or values.
;;; that is to say 'a 'b don't bind
(define-module (avector)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-43) ;; vector libraries
  #:export (vassociation-procedure
            vassoc vassq vassv

            vassociation-ref-procedure
            vassoc-ref vassq-ref vassv-ref

            vassociation-remove
            vassoc-remove vassq-remove vassv-remove

            vassociation-set!
            vassoc-set! vassq-set! vassv-set!

            vassociation-set
            vassoc-set vassq-set vassv-set

            avector
            avector-copy)
  #||#)
;; vassociation-procedure :: test * ref -> (('a * avec) -> assc)
(define (vassociation-procedure equalfn ref)
  (lambda (key avec)
    (let ((idx
           (vector-index
            (lambda (x) (and x (equalfn (ref x) key)))
            avec)))
      (and idx (vector-ref avec idx)))))

;; vass(q|v|oc) :: (('a * avec) -> assc)
(define vassq   (vassociation-procedure eq? car))
(define vassv   (vassociation-procedure eqv? car))
(define vassoc  (vassociation-procedure equal? car))

;; vrass(q|v|oc) :: (('a * avec) -> assc)
(define vrassq  (vassociation-procedure eq? cdr))
(define vrassv  (vassociation-procedure eqv? cdr))
(define vrassoc (vassociation-procedure equal? cdr))

;; vassociation-ref-procedure :: \ 
;;   vassociation-procedure * ref -> (('a * avec) -> 'a|'b)
(define (vassociation-ref-procedure vassocation-procedure ref)
  (lambda (key avec)
    (let ((val (vassocation-procedure key avec)))
      (and val (ref val)))))

;; vass(q|v|oc)-ref :: ('a * avec) -> 'b
(define vassq-ref   (vassociation-ref-procedure vassq  cdr)) 
(define vassv-ref   (vassociation-ref-procedure vassv  cdr))
(define vassoc-ref  (vassociation-ref-procedure vassoc cdr))

;;;; vass(q|v|oc)-ref :: ('a * avec) -> 'a
;;(define vrassq-key  (vassociation-ref-procedure vrassq  car))
;;(define vrassv-key  (vassociation-ref-procedure vrassv  car))
;;(define vrassoc-key (vassociation-ref-procedure vrassoc car))
;;; ^ proof of concept

;; vassociation-set! :: test * ref -> ('a * 'b * avec -> Mutation)
(define (vassociation-set! equalfn ref)
  (lambda (key newval avec)
    (let ((idx (vector-index (lambda (x) (equalfn (ref x) key)) avec)))
      (vector-set! avec idx (cons key newval)))))

;; vass(q|v|oc)-set! :: 'a * 'b * avec -> Mutation
(define vassq-set!  (vassociation-set! eq? car))
(define vassv-set!  (vassociation-set! eqv? car))
(define vassoc-set! (vassociation-set! equal? car))

;; vassociation-set :: test * ref -> ('a * 'b * avec -> avec)
(define (vassociation-set equalfn ref)
  (lambda (key newval avec)
    (vector-fold
      (lambda (i a x)
        (vector-append a
                       (vector
                         (if (equalfn (ref x) key)
                           (cons key newval)
                           x))))
      #()
      avec)))

;; vass(q|v|oc)-set :: 'a * 'b * avec -> avec
(define vassq-set  (vassociation-set eq? car))
(define vassv-set  (vassociation-set eqv? car))
(define vassoc-set (vassociation-set equal? car))

;; vassociation-remove :: test * ref -> ('a * avec -> avec)
(define (vassociation-remove equalfn ref)
  (lambda (key avec)
    (vector-fold
     (lambda (i a x)
       (if (equalfn (ref x) key)
           a
           (vector-append a (vector x))))
     #()
     avec)))

;; vass(q|v|oc)-remove :: 'a * avec -> avec
(define vassq-remove  (vassociation-remove eq? car))
(define vassv-remove  (vassociation-remove eqv? car))
(define vassoc-remove (vassociation-remove equal? car))

;; vassociation-remove :: test * ref -> ('a * avec -> Mutation)
#||
(define (vassociation-remove! equalfn ref)
  (lambda (key avec)
    ;; FIXME: this may need to be a macro
    ;; or use vector-set! type magic
    (set! avec ((vassociation-remove equalfn ref) key avec))))

;; vass(q|v|oc)-remove! :: 'a * avec -> Mutation
(define vassq-remove!  (vassociation-remove! eq? car))
(define vassv-remove!  (vassociation-remove! eqv? car))
(define vassoc-remove! (vassociation-remove! equal? car))
||#

;; vacons :: 'a * 'b * avec -> avec
(define (vacons key val avec)
  (vector-append (vector (cons key val)) avec))

;; avector-copy :: avec -> avec
(define avector-copy vector-copy)

;; avector :: 'a ^ 'b list -> avec 
(define (avector . args)
  (unless (even? (length args))
    (set! args (append args (list (list)))))
  (let lp ((vec (vector))
           (args args))   
    (if (null? args)
      vec
      (lp (vector-append vec ;; much garbage
                         (vector (apply cons (take args 2))))
          (cddr args)))))
