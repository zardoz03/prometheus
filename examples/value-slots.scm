(add-to-load-path (dirname (current-filename)))
(use-modules
 (prometheus)
 (prometheus-x))

(define o1 (*object* 'clone))
(o1 'name: "*o1*")

(format #t "$1: o1.value-slots:  ~a~%" (o1 'value-slots))

(o1 'add-implementor-message: 'foo)

(format #t "$2: o1.value-slots:  ~a~%" (o1 'value-slots))

(define o2 (o1 'clone))
(o2 'name: "*o2*")

(format #t "$1: o2.value-slots:  ~a~%" (o2 'value-slots))

(o2 'implement! 'foo (lambda (self resend) 'foo))

(format #t "all-implemented:     ~a~%" (o2 'check-if-all-implemented))
(format #t "$2: o2.value-slots:  ~a~%" (o2 'value-slots))
(format #t "o2.foo():            ~a~%" (o2 'foo))
