;;; https://gist.github.com/digikar99/b76964faf17b3a86739c001dc1b14a39#the-non-obvious-problems

;; - t. Madhavan Mukund's lecture notes 2004-04-29
;; viz. https://www.cmi.ac.in/~madhavan/courses/pl2009/lecturenotes/lecture-notes/node28.html
;;> In other words, we can implement queues and stacks in terms of deques, so as
;;  datatypes, Stack and Queue inherit from Deque. On the other hand, neither
;;  Stack not Queue are subtypes of Deque since they do not support all the
;;  functions provided by Deque. In fact, in this case, Deque is a subtype of
;;  both Stack and Queue!

(use-modules (hermes)
             (prometheus)
             (prometheus-x)
             (srfi srfi-43) ; vectors
             #||#)

(define (vector-member elt vec)
  (vector-index (lambda (x) (equal? x elt)) vec))

(let ((o *object*))
  (o 'add-value-slot! 'type 'type: #t)
  (o 'add-value-slot! 'subtypes 'subtypes: #())
  (o 'add-method-slot! 'subtype?
     (lambda (self resend type)
       (unless (symbol? type)
         (error (format #f "Type specifier is not a symbol! ~a" type)))
       ;; note this does not do a tree search on the elements of subtypes, yet
       (vector-member type (o 'subtypes)))))

(define *deque*
  (let ((o (*object* 'clone)))
    (o 'add-value-slot! '__seq__ '__seq__: #())
    (o 'add-message-slot! 'delete-front
       (lambda (self resend obj)
         obj))
    (o 'add-message-slot! 'insert-front
       (lambda (self resend obj)
         obj))
    (o 'add-message-slot! 'delete-rear
       (lambda (self resend obj)
         obj))
    (o 'add-message-slot! 'insert-rear
       (lambda (self resend obj)
         obj))
    o))

(define *stack*
  (let ((o (*deque* 'clone)))
    (vector-set! (o 'subtypes) 0 *deque*)
    (o 'delete-message! 'delete-rear)
    (o 'delete-message! 'insert-rear)
    o))

(define *queue*
  (let ((o (*deque* 'clone)))
    (vector-set! (o 'subtypes) 0 *deque*)
    (o 'delete-message! 'delete-rear)
    (o 'delete-message! 'insert-front)
    o))
;;; deque-stack-queue.scm ends here
